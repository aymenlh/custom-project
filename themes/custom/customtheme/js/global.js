jQuery(document).ready(function () {
  var lang = jQuery("html").attr("lang");

  /** annimation **/

  new WOW().init();
  /** fin annimation **/


  jQuery(".bef-datepicker").attr("autocomplete", "off");
  	
  jQuery(function() {
    jQuery( ".bef-datepicker" ).datepicker({
    altField: "#datepicker",
    closeText: 'Fermer',
    prevText: 'Précédent',
    nextText: 'Suivant',
    currentText: 'Aujourd\'hui',
    monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
    monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
    dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
    dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
    dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
    weekHeader: 'Sem.',
    dateFormat: 'yy-mm-dd'
    });
    });

  /** Menu Toggle  **/
  jQuery(".navbar-default .navbar-toggle").click(function () {
    var bloc_menu = jQuery(".main-menu");
    if (bloc_menu.is(":hidden")) {
      bloc_menu.slideDown();
      jQuery(this).addClass("active");
    } else {
      jQuery(this).removeClass("active");
      bloc_menu.slideUp();
    }
  });
  /** Fin Menu Toggle  **/

  jQuery("#search-block-form").validate({
    rules: {
        keys: {
            required: true,
            minlength: 3
        }

    }
});

  /** Search form  **/
  /*
  try {
    jQuery('.search_blc h2').on('click', function (e) {
        e.stopPropagation();
        e.preventDefault();
        jQuery('.search_blc  form').slideToggle(500);
       /* jQuery('.search_blc .form-text').slideToggle(500);
        jQuery('.search_blc .form-actions').slideToggle(500);*/
       /* jQuery(this).toggleClass('active');
        jQuery('header').toggleClass('search_toggled');
    });

    jQuery(".search_blc  form").on("click", function (e) {
        e.stopPropagation();
    });
  jQuery(document).on('click', function (e) {
        var target = jQuery(".search_blc  form");
        if ((target).is(":visible")) {
            target.hide();
            jQuery('.search_blc h2').removeClass('active');
        }
    });
    } catch (err) {
    }
*/
  /** magnificPopup **/
  try {
    if (jQuery(".link-popup-video").length) {
      jQuery(".link-popup-video").magnificPopup({
        type: "iframe",
        mainClass: "mfp-fade",
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false,
      });
    }
  } catch (err) {}

  try {
    if (jQuery(".link-popup-audio").length) {
      jQuery(".link-popup-audio").magnificPopup({
        type: "inline",
        midClick: true,
        callbacks: {
          close: function () {
            soundManager.stopAll();
            jQuery(".sm2-progress-ball").css("left", "0%");
            jQuery(".sm2-progress-bar").css("width", "0%");
            jQuery(".sm2-inline-time").text("0:00");
          },
        },
      });
    }
  } catch (err) {}
  /** magnificPopup **/

  /** masonry **/
  try {
    if (jQuery(".grid").length) {
      jQuery(".grid").masonry({
        // set itemSelector so .grid-sizer is not used in layout
        itemSelector: ".grid-item",
        // use element for option
        percentPosition: true,
      });
    }
  } catch (err) {}

  /** masonry **/
  try {
    if (jQuery(".slider-one-item").length) {
      var carouseldots1 = jQuery(".slider-one-item");
      carouseldots1.owlCarousel({
        items: 1,
        rtl: lang == "ar" ? true : false,
        nav: true,
        loop: true,
        dots: true,
      });
    }
  } catch (err) {}


  try {
    if (jQuery(".slider-four-item").length) {
      var carouseldots4 = jQuery(".slider-four-item");
      carouseldots4.owlCarousel({
        items: 1,
        rtl: lang == "ar" ? true : false,
        nav: true,
        loop: false,
        dots: false,
        margin: 32,
        responsive: {
          0: {
            items: 1,
          },
          480: {
            items: 2,
          },
          768: {
            items: 3,
          },
          991: {
            items: 4,
          },
        },
      });
    }
  } catch (err) {}


  

  try {
    if (jQuery(".slider-tow-item").length) {
      var carouseldots2 = jQuery(".slider-tow-item");
      carouseldots2.owlCarousel({
        items: 1,
        rtl: lang == "ar" ? true : false,
        nav: true,
        loop: false,
        autoplay:true,
        dots: false,
        margin: 32,
        responsive: {
          0: {
            items: 1,
          },
          480: {
            items: 1,
          },
          768: {
            items: 2,
          },
          991: {
            items: 2,
          },
        },
      });
    }
  } catch (err) {}
  try {
    if (jQuery(".carousel-banniere").length) {
      var carouselBanner = jQuery(".carousel-banniere");
      carouselBanner.owlCarousel({
        items: 1,
        rtl: lang == "ar" ? true : false,
        nav: false,
        loop: true,
        dots: true,
        lazyLoad: true,
      });

      function setAnimation(_elem, _InOut) {
        var animationEndEvent =
          "webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend";
        _elem.each(function () {
          var jQueryelem = jQuery(this);
          var jQueryanimationType =
            "animated " + jQueryelem.data("animation-" + _InOut);
          jQueryelem
            .addClass(jQueryanimationType)
            .one(animationEndEvent, function () {
              jQueryelem.removeClass(jQueryanimationType);
            });
        });
      }

      carouselBanner.on("change.owl.carousel", function (event) {
        var jQuerycurrentItem = jQuery(".owl-item", carouselBanner).eq(
          event.item.index
        );
        var jQueryelemsToanim = jQuerycurrentItem.find("[data-animation-out]");
        setAnimation(jQueryelemsToanim, "out");
      });

      carouselBanner.on("changed.owl.carousel", function (event) {
        var jQuerycurrentItem = jQuery(".owl-item", carouselBanner).eq(
          event.item.index
        );
        var jQueryelemsToanim = jQuerycurrentItem.find("[data-animation-in]");
        setAnimation(jQueryelemsToanim, "in");
      });
    }
  } catch (err) {}

  try {
    if (jQuery(".owl-actualite").length) {
      var carouselDomaine = jQuery(".owl-actualite");
      carouselDomaine.owlCarousel({
        rtl: lang == "ar" ? true : false,
        nav: false,
        loop: true,
        dots: false,
        margin: 32,
        responsive: {
          0: {
            items: 1,
          },

          768: {
            items: 2,
          },
          992: {
            items: 3,
          },

          1200: {
            items: 4,
          },
        },
      });
    }
  } catch (err) {}

  try {
    if (jQuery(".owl-gallery").length) {
      var carouselDomaine = jQuery(".owl-gallery");
      carouselDomaine.owlCarousel({
        nav: true,
        navText: [
          '<i class="fal fa-long-arrow-left"></i>',
          '<i class="fal fa-long-arrow-right"></i>',
        ],
        loop: true,
        dots: false,
        margin: 16,
        responsive: {
          0: {
            items: 1,
          },
          768: {
            items: 2,
          },
          991: {
            items: 3,
          },
        },
      });
    }
  } catch (err) {}

  if (jQuery("#views-exposed-form-recherche-page-1").length) {
    jQuery("#views-exposed-form-recherche-page-1").validate({
      rules: {
        t: {
          required: true,
          minlength: 3,
        },
      },
    });
  }

  if (jQuery(".form_contact").length) {
    var lang2 = jQuery("html").attr("lang");
    var msg;
    var msg1;
    var msg2;
    var msg3;

    if (lang2 == "ar") {
      (msg1 = "الرجاء إدخال أحرف فقط"),
        (msg2 = "رقم الهاتف غير صحيح"),
        (msg3 = "رجاء إدخال عنوان بريد إلكتروني صحيح"),
        (msg = "هذا الحقل إلزامي");
    } else if (lang2 == "fr") {
      (msg1 = "Veuillez saisir des lettres seulement."),
        (msg2 = "Numéro de téléphone non valide"),
        (msg3 = "Veuillez fournir une adresse électronique valide."),
        (msg = "Ce champ est obligatoire.");
    }

    jQuery.validator.addMethod(
      "fnType",
      function (value, element) {
        return value.match(/^[- +()]*[0-9][- +()0-9]*$/);
      },
      msg2
    );

    jQuery.validator.addMethod(
      "lettersonly",
      function (value, element) {
        return this.optional(element) || /^[a-zA-Z\s]+$/i.test(value);
      },
      msg1
    );

    jQuery.validator.addMethod(
      "isemail",
      function (value, element) {
        return (
          this.optional(element) ||
          /^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,6}$/i.test(value)
        );
      },
      msg3
    );

    jQuery(".form_contact").validate({
      rules: {
        nom: {
          lettersonly: true,
        },
        prenom: {
          lettersonly: true,
        },
        email: {
          isemail: true,
        },
        nationalite: {
          required: true,
        },

        telephone: {
          number: true,
          fnType: true,
          minlength: 8,
        },
      },

      submitHandler: function (form) {
        if (grecptcha.getResponse()) {
          form.submit();
        } else {
          jQuery(".captcha").append(
            '<br><label class="error " >' + msg + "</label>"
          );
        }
      },
    });
  }

  /**  Form Reclamation **/
  if (jQuery(".form_reclamation").length) {
    jQuery(".form_reclamation").validate({
      rules: {
        nom_et_prenom: {
          required: true,
        },
        email: {
          required: true,
          email: true,
        },
        telephone: {
          required: true,
          number: true,
          minlength: 8,
        },
        ma_reclamation_concerne: {
          required: true,
        },
        sujet_de_votre_reclamation: {
          required: true,
          minlength: 3,
        },
      },
      messages: {
        nom_et_prenom: {
          required: "Ce champ est obligatoire.",
        },
        email: {
          required: "Ce champ est obligatoire.",
          email: "Veuillez saisir une adresse email valide",
        },
        telephone: {
          required: "Ce champ est obligatoire.",
          number: "Veuillez saisir un nombre valide",
          minlength: "Veuillez saisir au moins 8 caractères",
        },
        ma_reclamation_concerne: {
          required: "Ce champ est obligatoire.",
        },
        sujet_de_votre_reclamation: {
          required: "Ce champ est obligatoire.",
          minlength: "Veuillez saisir au moins 3 caractères",
        },
      },
      submitHandler: function (form) {
        if (grecptcha.getResponse()) {
          form.submit();
        } else {
          jQuery(".captcha").append(
            '<br><label class="error " >' + msg + "</label>"
          );
        }
      },
    });
  }

  /**  Form Condidature **/
  if (jQuery(".form_condidature").length) {
    jQuery(".form_condidature").validate({
      rules: {
        nom_et_prenom: {
          required: true,
        },
        email: {
          required: true,
          email: true,
        },
        telephone: {
          required: true,
          number: true,
          minlength: 8,
        },
        poste_convoite: {
          required: true,
        },
      },
      messages: {
        nom_et_prenom: {
          required: "Ce champ est obligatoire.",
        },
        email: {
          required: "Ce champ est obligatoire.",
          email: "Veuillez saisir une adresse email valide",
        },
        telephone: {
          required: "Ce champ est obligatoire.",
          number: "Veuillez saisir un nombre valide",
          minlength: "Veuillez saisir au moins 8 caractères",
        },
        poste_convoite: {
          required: "Ce champ est obligatoire.",
        },
      },
      submitHandler: function (form) {
        if (grecptcha.getResponse()) {
          form.submit();
        } else {
          jQuery(".captcha").append(
            '<br><label class="error " >' + msg + "</label>"
          );
        }
      },
    });
  }

  /**  Form Evenement **/
  if (jQuery(".form_evenement").length) {
    jQuery(".form_evenement").validate({
      rules: {
        nom_et_prenom: {
          required: true,
        },
        email: {
          required: true,
          email: true,
        },
        telephone: {
          required: true,
          number: true,
          minlength: 8,
        },
        evenement: {
          required: true,
        },
      },
      messages: {
        nom_et_prenom: {
          required: "Ce champ est obligatoire.",
        },
        email: {
          required: "Ce champ est obligatoire.",
          email: "Veuillez saisir une adresse email valide",
        },
        telephone: {
          required: "Ce champ est obligatoire.",
          number: "Veuillez saisir un nombre valide",
          minlength: "Veuillez saisir au moins 8 caractères",
        },
        evenement: {
          required: "Ce champ est obligatoire.",
        },
      },
      submitHandler: function (form) {
        if (grecptcha.getResponse()) {
          form.submit();
        } else {
          jQuery(".captcha").append(
            '<br><label class="error " >' + msg + "</label>"
          );
        }
      },
    });
  }

  /**  Form Newsletter **/
  /**  Form Newsletter **/
  if (jQuery(".form_newsletter").length) {
    jQuery(".form_newsletter").validate({
      rules: {
        inscription_a_la_newsletter: {
          required: true,
          email: true,
        },
      },
      messages: {
        inscription_a_la_newsletter: {
          required: "Ce champ est obligatoire.",
          email: "Veuillez saisir une adresse email valide",
        },
      },
    });
  }

/** For audio player **/
if (jQuery('.link-popup-audio').length) {
  jQuery('.link-popup-audio').magnificPopup({
    type: 'inline',
    midClick: true,
    callbacks: {
      close: function(){
        soundManager.stopAll();
        jQuery('.sm2-progress-ball').css('left','0%');
        jQuery('.sm2-progress-bar').css('width','0%');
        jQuery('.sm2-inline-time').text('0:00');
      }
      
    }
  });
  }


});



(function ($, Drupal) {
  "use strict";
  Drupal.behaviors.mytheme_view_glossaire_scripts = {
    attach: function (context, settings) {
        /** Nice select  **/
  jQuery(".default-filter select").niceSelect();
  $( document ).ajaxComplete(function () {
    jQuery(".default-filter select").niceSelect();
  });
      if (window.mytheme_view_glossaire_scripts_inited) {
        return;
      }
      window.mytheme_view_glossaire_scripts_inited = true;
      $(document).ready(function () {
        Drupal.behaviors.mytheme_view_glossaire_scripts.update_filters();
      });

      $(document).on(
        "click",
        ".view-style-glossaire .filters-alphabet-glossaire .item-alphabet-link ",
        function (event) {
          event.preventDefault();
          var value = $(this).attr("data-value");
          $(".select-wrapper select").val(value);
          $(".view-style-glossaire .views-exposed-form .form-submit ").click();
        }
      );
      $(document).ajaxComplete(function () {
        Drupal.behaviors.mytheme_view_glossaire_scripts.update_filters();
      });
    },

    update_filters: function (context, settings) {
      if (!$(".view-style-glossaire").length) {
        return;
      }

      $(".view-style-glossaire .filters-alphabet-glossaire").remove();

      var html_ =
        '<div class="filters-alphabet-glossaire" ><ul class="list-alphabet" >';

      $(".select-wrapper select option").each(function (key, item) {
        if ($(item).val()) {
          html_ +=
            '<li class="item-alphabet-li" ><a class="item-alphabet-link ' +
            ($(item).val() === $(".select-wrapper select").val()
              ? "selected"
              : "") +
            '" href="#" data-value="' +
            $(item).val() +
            '" >' +
            $(item).text() +
            "</a></li>";
        }
      });
      html_ += "</ul></div>";
      $(".view-style-glossaire .view-filters form").append(html_);
    },
  };
  $(".theme-news").each(function (element) {
    var color = $(this).attr('data-bg');
    $(this).css({ background: color });
    
  });

  if($('#navbar-main').text().replace(/ /g,'').replace(/\s+/, "").replace(/\s/g, "")==""){
    $('#navbar-main').remove();
  }
  jQuery(".menu-secondaire .we-mega-menu-li[data-level='0']").addClass("sub-menu-mobile");
  jQuery(".menu-secondaire .we-mega-menu-li[data-level='0'] >.we-mega-menu-submenu").addClass("submenu");
  jQuery(".menu-secondaire .submenu >.we-mega-menu-submenu-inner").addClass("container");
  
  
 /**We should add specific class to accordion to obtain once element open  --- oneElementshown -- **/ 
  $(".oneElementshown .panel-heading .panel-title > a").on("click",function(e){
    e.preventDefault();
    var target = $(this).parents(".card-header.panel-heading").next('.panel-collapse');
    if(target.hasClass("show")){   
      target.collapse("hide");
      $(".panel-collapse").collapse("hide");
      e.stopPropagation();
    }else{
      $(".panel-collapse").collapse("hide");  
      target.collapse("show");      
      e.stopPropagation();
    } 
  });

})(jQuery, Drupal);

Drupal.behaviors.ajaxowlModule = {
  attach: function (context, settings) {
  var isrRtl  = false;
  var textlang = jQuery("html").attr("lang");
  if( textlang == "ar"){
    isrRtl = true;
  }
  
    if (jQuery(".slider-thre-item").length) {
      
        var carouseldots3 = jQuery(".slider-thre-item");
        carouseldots3.owlCarousel({
          items: 3,
          rtl: isrRtl,
          nav: true,
          loop: false,
          dots: true,
          lazyLoad:true,
          margin: 32,
          responsive: {
            0: {
              items: 1,
            },
            480: {
              items: 1,
            },
            768: {
              items: 2,
            },
            991: {
              items: 3,
            },
          },
        });


    setTimeout(function() { 
      carouseldots3.trigger('refresh.owl.carousel');
    }, 2000);
  
    }


    if (jQuery(".link-popup-video").length) {
      jQuery(".link-popup-video").magnificPopup({
        type: "iframe",
        mainClass: "mfp-fade",
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false,
      });
    }

  }
};