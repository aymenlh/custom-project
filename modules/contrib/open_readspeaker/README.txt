; Module name
Open ReadSpeaker

; Drupal Version
Drupal 8.x

; Description
This module integrates ReadSpeaker Enterprise Highlighting to your 
Drupal 8 website

; Prerequisites
An own account at ReadSpeaker that includes 
the ReadSpeaker Enterprise Highlighting product.

; Configuration
To enable this module, visit module page
and enable Open ReadSpeaker Enterprise Highlighting module.
You can configure /admin/config/service/openreadspeakersettings

; Author
takim
