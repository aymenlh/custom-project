<?php

namespace Drupal\open_readspeaker\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides a 'OpenReadspeakerBlock' block.
 *
 * @Block(
 *  id = "open_readspeaker_block",
 *  admin_label = @Translation("Open readspeaker block"),
 * )
 */
class OpenReadspeakerBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Admin config object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition, $container->get('config.factory'));
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $form['open_readspeaker_buttonstyle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Class of button'),
      '#description' => $this->t('Write class name of the button. You can add multiple calsses by space.'),
      '#default_value' => isset($this->configuration['open_readspeaker_buttonstyle']) ? $this->configuration['open_readspeaker_buttonstyle'] : 'open-readspeaker-button',
    ];

    $form['open_readspeaker_buttontext'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button text'),
      '#description' => $this->t('Please write button text here.'),
      '#default_value' => isset($this->configuration['open_readspeaker_buttontext']) ? $this->configuration['open_readspeaker_buttontext'] : $this->t('Listen'),
    ];

    $form['open_readspeaker_buttontitle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button title'),
      '#description' => $this->t('Specify the title attribute for the ReadSpeaker button.'),
      '#default_value' => isset($this->configuration['open_readspeaker_buttontitle']) ? $this->configuration['open_readspeaker_buttontitle'] : $this->t('Listen to this page using ReadSpeaker'),
    ];

    $form['open_readspeaker_reading_area'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reading area ID'),
      '#description' => $this->t('Specify content using HTML ID attribute.'),
      '#default_value' => isset($this->configuration['open_readspeaker_reading_area']) ? $this->configuration['open_readspeaker_reading_area'] : '',
      '#required' => TRUE,
    ];

    $form['open_readspeaker_reading_area_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Reading area of classes'),
      '#description' => t('Specify content using HTML Class attribute(s). For multiple classes use format: class1,class2,class3'),
      '#default_value' => isset($this->configuration['open_readspeaker_reading_area_class']) ? $this->configuration['open_readspeaker_reading_area_class'] : '',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['open_readspeaker_buttonstyle'] = $form_state->getValue('open_readspeaker_buttonstyle');
    $this->configuration['open_readspeaker_buttontext'] = $form_state->getValue('open_readspeaker_buttontext');
    $this->configuration['open_readspeaker_buttontitle'] = $form_state->getValue('open_readspeaker_buttontitle');
    $this->configuration['open_readspeaker_reading_area'] = $form_state->getValue('open_readspeaker_reading_area');
    $this->configuration['open_readspeaker_reading_area_class'] = $form_state->getValue('open_readspeaker_reading_area_class');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configFactory->get('open_readspeaker.settings');
    $accountid = $config->get('open_readspeaker_accountid');
    $post_mode = $config->get('open_readspeaker_post_mode');
    $url = Url::fromRoute(
      '<current>',
      \Drupal::request()->query->all(),
      ['absolute' => TRUE]
    )->toString();

    $build = [];
    if (empty($accountid)) {
      $this->messenger()->addMessage($this->t('Please go to @link and fill the account id.', [
        '@link' => Link::createFromRoute($this->t('manage open ReadSpeaker'), 'open_readspeaker.settings')->toString(),
      ]));
      return $build;
    }
    if ($post_mode) {
      $library[] = 'open_readspeaker/post_mode';
    }
    $library[] = 'open_readspeaker/basic';

    $build['open_readspeaker_block'] = [
      '#theme' => 'open_readspeaker_ui',
      '#accountid' => $accountid,
      '#open_readspeaker_i18n' => $config->get('open_readspeaker_i18n'),
      '#request_path' => $url,
      '#custom_style' => isset($this->configuration['open_readspeaker_buttonstyle']) ? $this->configuration['open_readspeaker_buttonstyle'] : 'open-readspeaker-button',
      '#button_text' => isset($this->configuration['open_readspeaker_buttontext']) ? $this->configuration['open_readspeaker_buttontext'] : $this->t('Listen'),
      '#button_title' => isset($this->configuration['open_readspeaker_buttontitle']) ? $this->configuration['open_readspeaker_buttontitle'] : $this->t('Listen to this page using ReadSpeaker'),
      '#open_readspeaker_reading_area' => isset($this->configuration['open_readspeaker_reading_area']) ? $this->configuration['open_readspeaker_reading_area'] : 'rs_read_this',
      '#open_readspeaker_reading_area_class' => isset($this->configuration['open_readspeaker_reading_area_class']) ? $this->configuration['open_readspeaker_reading_area_class'] : 'rs_read_this_class',
      '#attached' => [
        'library' => $library,
      ],
      '#cache' => [
        'contexts' => [
          'url',
        ],
      ],
    ];
    return $build;
  }

}
