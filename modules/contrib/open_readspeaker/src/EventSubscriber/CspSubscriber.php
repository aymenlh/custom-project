<?php

namespace Drupal\open_readspeaker\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\csp\CspEvents;
use Drupal\csp\Event\PolicyAlterEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class CspPolicyAlterSubscriber.
 */
class CspSubscriber implements EventSubscriberInterface {

  /**
   * Read only config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The CspSubscriber constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('open_readspeaker.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    if (!class_exists(CspEvents::class)) {
      return [];
    }

    return [
      CspEvents::POLICY_ALTER => 'onCspPolicyAlter',
    ];
  }

  /**
   * Alter CSP policy for tracking requests.
   *
   * @param \Drupal\csp\Event\PolicyAlterEvent $alterEvent
   *   The policy alter event.
   */
  public function onCspPolicyAlter(PolicyAlterEvent $alterEvent) {
    $policy = $alterEvent->getPolicy();
    $trackingDomain = parse_url($this->config->get('open_readspeaker_url'), PHP_URL_HOST);

    $policy->fallbackAwareAppendIfEnabled('style-src', [$trackingDomain]);
    $policy->fallbackAwareAppendIfEnabled('style-src-elem', [$trackingDomain]);
  }

}
