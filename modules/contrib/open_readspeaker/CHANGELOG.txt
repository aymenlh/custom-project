Open ReadSpeaker 8.x-1.x-dev, xxxx-xx-xx
------------------------------------

Open ReadSpeaker 8.x-1.8, 2021-12-08
------------------------------------
#3253242 by sunlix, Bjoern: JS Dependencies in POST-Mode

Open ReadSpeaker 8.x-1.8, 2021-11-19
------------------------------------
#3250061 by sunlix: Compatibility to CSP
#3247605 by sunlix, JoshaHubbers: Rework: Add an option to include
  url parameters in the request_path

Open ReadSpeaker 8.x-1.7, 2021-07-23
------------------------------------
#3225053 by sunlix: Add TugboatQA
#3223988 by JoshaHubbers, sunlix: Add an option to include url parameters in the request_path

Open ReadSpeaker 8.x-1.6, 2020-06-17
------------------------------------
#3152385 by sunlix: Improve labels for better readability
#3146505 by sunlix: OpenReadspeakerBlock: refactor $request_path generation
  along with template improvements
#3150523 by sunlix: Make ReadSpeaker asset URL configurable using tokens

Open ReadSpeaker 8.x-1.5, 2020-06-10
------------------------------------
#3149592 by sunlix: Update composer.json
#2936239 by sunlix, mpp, ressa: All text disappear after collapse of player,
  replaced by "undefined"
#3146504 by sunlix: Remove unnecessary and unused config 'open_readspeaker'
#3146494 by sunlix: OpenReadspeakerBlock: Provide PHPDoc for constructor
#3145763 by sunlix: PHPCS Audit
#3080285 by sunlix, askibinski: Rename development mode option to use POST
#2910272 by mpp, Aleksey Pavlyuk, askibinski, sunlix, idebr, mErilainen:
  Add translation support
#3032999 by idebr, sunlix, mpp: Make button title attribute configurable
#3039057 by Dimiter, sunlix: ReadSpeaker JS library cannot be overridden
  by other modules
#3068617 by mpp, sunlix, ducktape: Add support for Dutch (Belgian) voice
#2989136 by maximpodorov, mpp, sunlix: Incorrect cache tags in block
  render array
#2920724 by markusbroman: Call to undefined function
#3090082 by sunlix, Sergiu Stici: Deprecated Code Report
#3124028 by Suresh Prabhu Parkala, sunlix: Drupal 9 compatibility

Open ReadSpeaker 8.x-1.4, 2017-08-30
------------------------------------
#2903787 by keopx, takim: Cache-Tags use module name cache name
#2903792 by keopx, takim: Cache context by url to render block by url
