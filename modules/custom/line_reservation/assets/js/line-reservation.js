/**
 * @file
 * Attaches behaviors for the custom Google Maps.
 */

 (function ($, Drupal) {

     Drupal.behaviors.reservation = {
         attach: function (context, settings) {
             jQuery('#my_form_wrapper #edit-phone-confirm').on("cut", function (e) {
                 e.preventDefault();
             });
             jQuery('#my_form_wrapper #edit-phone-confirm').on("copy", function (e) {
                 e.preventDefault();
             });
             jQuery('#my_form_wrapper #edit-phone-confirm').on("paste", function (e) {
                 e.preventDefault();
             });


             jQuery('#edit-line-prefix-id').select2();
             jQuery('#edit-line-category-id').select2();

             jQuery('#edit-number').select2({
                allowClear: false,
                cache: false,
                ajax: {
                  type: 'GET',
                  url:  "reserver-mon-numero-accept",
                  contentType: 'application/json; charset=utf-8',
                  dataType: 'json',
                    data: function (params) {
                        var prefix_id = jQuery('#edit-line-prefix-id').val();
                        var category_id = jQuery('#edit-line-category-id').val();
                        console.log(prefix_id, category_id);
                    return {
                      prefix_id: prefix_id,
                      category_id: category_id,
                      term: params.term || '',
                      page: params.page || 1,
                      rows: 20
                    }
                  },
                  processResults: function (res, params) {
                    if (res.status == 'ok') {
                      var lines = res.data;
                      params.page = params.page || 1;
                      var data = [],
                        i;
                      for (i = 0; i < lines.length; i++) {
                        data.push({
                          id: lines[i].id,
                          text: lines[i].text
                        });
                      }
                        console.log(data);
                      return {
                        results: data,
                        pagination: {
                          more: params.page < res.data.last_page,
                        }
                      };

                    } else {
                      console.log('Failed to fetch the addresse');
                    }
                  }
                }
              });

             jQuery('#my_form_wrapper input[type="number"]').bind("keypress", function (evt) {
                 /*if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57) {
                     evt.preventDefault();
                 }*/
             });
         }
     }
})(jQuery, Drupal);
