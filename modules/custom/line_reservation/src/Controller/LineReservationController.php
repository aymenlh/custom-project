<?php

namespace Drupal\line_reservation\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for line_reservation routes.
 */
class LineReservationController extends ControllerBase
{
    /**
     * Builds the response.
     */
    public function build()
    {
        $term = \Drupal::request()->query->get('term');
        $page = \Drupal::request()->query->get('page');
        $rows = \Drupal::request()->query->get('rows');
        $prefix_id = \Drupal::request()->query->get('prefix_id');
        $category_id = \Drupal::request()->query->get('category_id');

        $endpoint_services = \Drupal::service('line_reservation.endpoint_services');
        $available_number = $endpoint_services->getAvailableNumber($prefix_id,$category_id);

        $result = [
            'status' => 'ko',
            'data' => [],
          ];
          if ($available_number) {
            $result['status'] = 'ok';
            $result['data'] = $available_number;
          }
          echo json_encode($result);die;
    }
    /**
     * Builds the response.
     */
    public function getAvailableNumber(){
        $term = \Drupal::request()->query->get('term');
        $page = \Drupal::request()->query->get('page');
        $rows = \Drupal::request()->query->get('rows');
        $prefix_id = \Drupal::request()->query->get('prefix_id');
        $category_id = \Drupal::request()->query->get('category_id');

        $endpoint_services = \Drupal::service('line_reservation.endpoint_services');
        $available_number = $endpoint_services->getAvailableNumber($prefix_id,$category_id);

        $result = [
            'status' => 'ko',
            'data' => [],
          ];
          if ($available_number) {
            $result = $available_number;
          }
          echo json_encode($result);die;
    }
}
