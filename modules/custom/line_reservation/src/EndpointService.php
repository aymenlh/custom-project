<?php

namespace Drupal\line_reservation;

use Drupal\Core\Session\AccountInterface;

/**
 * Class EndpointService
 * @package Drupal\line_reservation\Services
 */
class EndpointService {

  protected $currentUser;

  /**
   * CustomService constructor.
   * @param AccountInterface $currentUser
   */
  public function __construct(AccountInterface $currentUser) {
    $this->currentUser = $currentUser ?? NULL;
  }


  /**
   * @return \Drupal\Component\Render\MarkupInterface|string
   */
  public function getData() {
    return $this->currentUser->getDisplayName();
  }

   /**
     * Get List of categories from endpoint
     */
    public function getCategories()
    {
        $uri = 'api/v1/line-booking-noauth/get-lines-categories';
        $response = $this->sendRequest($uri, [], true, 'GET');
        $result = json_decode($response, true);
        $return = [
            '' => 'Choisir une catégorie'
        ];
        if($result['status']  && $result['data']){
            foreach ($result['data'] as $value) {
                $return[$value['id']] = $value['title'].' - '.$value['price'].' MRU';
            }
        }
        return $return;
    }
    /**
     * Get List of prefix from endpoint
     */
    public function getPrefixe()
    {
        $uri = 'api/v1/line-booking-noauth/get-lines-prefixes';
        $response = $this->sendRequest($uri, [], true, 'GET');
        $result = json_decode($response, true);
        $return = [
            '' => 'Choisir un préfixe'
        ];

        if($result['status']  && $result['data']){
            foreach ($result['data'] as $value) {
                $return[$value['id']] = $value['prefix'];
            }
        }
        return $return;
    }
    /**
     * Get List of prefix from endpoint
     */
    public function getAvailableNumber($prefix_id = NULL,$category_id = NULL)
    {
        $uri = 'api/v1/line-booking-noauth/get-available-lines?line_prefix_id='.$prefix_id.'&line_category_id='.$category_id;
        $response = $this->sendRequest($uri, [], true, 'GET');
        $result = json_decode($response, true);
        $return = [
            ['' => 'Choisir un numéro']
        ];
        if($result['status']  && isset($result['data']['availble_numbers']) && count($result['data']['availble_numbers'])>0){
            foreach ($result['data']['availble_numbers'] as $value) {
                array_push($return, ['id'=>$value,'text'=>$value]);
            }
        }
        return $return;
    }
    /**
     * Get List of prefix from endpoint
     */
    public function bookLine($line_prefix_id,$line_category_id,$number,$number_client)
    {
        $uri = 'api/v1/line-booking-noauth/book-line';
        $body = [
            'line_prefix_id' => $line_prefix_id,
            'line_category_id' => $line_category_id,
            'number' => $number,
            'number_client' => $number_client,
        ];
        $response = $this->sendRequest($uri, $body, true, 'POST');
        $result = json_decode($response, true);
        return $result;
    }


  private function sendRequest($path, $body = [], $withAuth = true, $method = 'GET')
    {
        $url_endpoint = \Drupal::config('line_reservation.settings')->get('url_endpoint');
        $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $uri = $url_endpoint.$path;
        $ch = curl_init($uri);
        $headers = [
          'source: web',
          'Content-Type: application/json',
          'Accept: application/json',
          'language: '.$language,
        ];
        if ($withAuth) {
            array_push($headers, 'Authorization: Bearer '.$this->loginServer());
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ('POST' == $method) {
            if ($body) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
            }
        } else {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }
        $return = curl_exec($ch);

        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if (500 == $statusCode) {
            return false;
        }

        return $return;
    }

    private function loginServer()
    {
        $username = \Drupal::config('line_reservation.settings')->get('login');
        $password = \Drupal::config('line_reservation.settings')->get('password');
        $body = [
          'email' => $username,
          'password' => $password,
        ];
        $return = $this->sendRequest('api/v1/login', $body, false, 'POST');
        if ($return) {
            $return = json_decode($return, true);
            return $return['data']['token'] ?? false;
        }
        return false;
    }

}
