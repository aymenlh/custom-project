<?php

namespace Drupal\line_reservation\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form line reservation for this site.
 */
class LineReservationForm extends FormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'line_reservation';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $endpoint_services = \Drupal::service('line_reservation.endpoint_services');
        $categories = $endpoint_services->getCategories();
        $prefixes = $endpoint_services->getPrefixe();

        $form['phone'] = [
                '#type' => 'number',
                '#required' => true,
                '#title' => $this->t('Votre numéro de téléphone'),
                '#placeholder' => $this->t('Saisir votre numéro de téléphone'),
                '#default_value' => $this->config('line_reservation.settings')->get('phone'),
                '#prefix' => '<div class="row"><div class="col-md-6">',
                '#suffix' => '<div id="message-phone"></div></div>',
        ];
        $form['phone_confirm'] = [
            '#type' => 'number',
            '#required' => true,
            '#title' => $this->t('Confirmer votre numéro de téléphone'),
            '#placeholder' => $this->t('Confirmer votre numéro de téléphone'),
            '#default_value' => $this->config('line_reservation.settings')->get('phone_confirm'),
            '#prefix' => '<div class="col-md-6">',
            '#suffix' => '<div id="message-confirm-phone"></div></div></div>',
        ];
        $form['line_prefix_id'] = [
            '#type' => 'select',
            '#title' => $this->t('Préfix'),
            '#required' => true,
            '#options' => $prefixes,
            '#prefix' => '<div class="row"><div class="col-md-6">',
            '#suffix' => '<div id="message-prefix"></div></div>',
        ];
        $form['line_category_id'] = [
            '#type' => 'select',
            '#title' => $this->t('Catégorie'),
            '#required' => true,
            '#options' => $categories,
            '#prefix' => '<div class="col-md-6">',
            '#suffix' => '<div id="message-categorie"></div></div></div>',
        ];
        $form['number'] = [
            '#type' => 'select',
            '#required' => true,
            '#placeholder' => $this->t('Choisir un numéro'),
            '#options' => [['' => $this->t('Choisir un numéro')] ],
            '#default_value' => '',
            '#validated' => TRUE,
            '#prefix' => '<div class="row"><div class="col-md-6">',
            '#suffix' => '<div id="message-number"></div></div></div>',
            '#states' => [
              'visible' => [
                  ':input[name="line_category_id"]' => [
                      ['!value' => ''],
                  ],
                  ':input[name="line_prefix_id"]' => [
                    ['!value' => ''],
                    ],
                ],
              ],
        ];
        $form['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Je valide'),
            '#attributes' => ['class' => ['bouton-blue']],
            '#button_type' => 'primary',
        ];
        $form['#attached'] = ['library' => 'line_reservation/line_reservation'];

        return [
            '#theme' => 'reservation_form',
            'render element' => $form,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        if (empty($form_state->getValue('phone')) || strlen($form_state->getValue('phone')) != 8) {
            $message = '<div class="error">'.$this->t('Le champ numéro de téléphone est obligatoire.').'</div>';
            $form_state->setErrorByName('phone', $message);
        } elseif ($form_state->getValue('phone') != $form_state->getValue('phone_confirm')) {
            $message = '<div class="error">'.$this->t('Les champs numéro de téléphone doivent être identique.').'</div>';
            $form_state->setErrorByName('phone_confirm', $message);
        }
        if (empty($form_state->getValue('line_prefix_id') )) {
            $message = '<div class="error">'.$this->t('Le champs préfix est obligatoire .').'</div>';
            $form_state->setErrorByName('phone_confirm', $message);
        }
        if (empty($form_state->getValue('line_category_id') )) {
            $message = '<div class="error">'.$this->t('Le champs catégorie est obligatoire.').'</div>';
            $form_state->setErrorByName('phone_confirm', $message);
        }
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $line_prefix_id = $form_state->getValue('line_prefix_id');
        $number_client = $form_state->getValue('phone');
        $line_category_id = $form_state->getValue('line_category_id');
        $number = $form_state->getValue('number');
        $endpoint_services = \Drupal::service('line_reservation.endpoint_services');
        $result = $endpoint_services->bookLine($line_prefix_id,$line_category_id,$number,$number_client);
        if($result['status']){
            \Drupal::messenger()->addMessage($this->t($result['message']), 'status', TRUE);
        }else{
            \Drupal::messenger()->addMessage($this->t($result['message']), 'status', FALSE);
        }
        $form_state->setRebuild(TRUE);
    }


}

?>
