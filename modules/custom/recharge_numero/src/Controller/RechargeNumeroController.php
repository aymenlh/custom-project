<?php

namespace Drupal\recharge_numero\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for recharge_numero routes.
 */
class RechargeNumeroController extends ControllerBase
{
    /**
     * Builds the response.
     */
    public function build()
    {
      $pid = \Drupal::request()->query->get('pid');

      if (!$pid) {
        return $this->redirect('<front>');
        exit();
      }
      $uri = 'api/v1/payment/check-payment-status';
      $dataMattel = [
        'pid' => $pid
      ];
      $return = $this->sendRequest($uri, $dataMattel, true, 'POST');
      $return = json_decode($return,true); 
      $payement_status  = false;
      $payement_message = 'Paiement en cours de vérification';
      if(isset($return['status']) && $return['status'] ){
        $payement_status  = $return["data"] ["status"];
        $payement_message =$return['message'];
      }elseif(isset($return['status']) && !$return['status']){
        $payement_status  = $return["data"] ["status"];
        $payement_message = $return['message'] ?? $payement_message;
      }

        $build['content'] = [
            '#theme' => 'recharge_callback',
            '#data' => [
              'payement_status' => $payement_status,
              'payement_message' => $payement_message
            ],
            '#custom_text' => $this->t('Implement method: CustomPage using a template'),
            '#attached' => ['library' => 'recharge_numero/recharge_numero'],
        ];

        return $build;
    }


    private function sendRequest($path, $body = [], $withAuth = true, $method = 'GET')
    {
        $url_endpoint = \Drupal::config('recharge_numero.settings')->get('url_endpoint');
        $uri = $url_endpoint.$path;
        $ch = curl_init($uri);
        $headers = [
          'source: web',
          'Content-Type: application/json',
          'Accept: application/json',
          'language: fr',
        ];
        if ($withAuth) {
            array_push($headers, 'Authorization: Bearer '.$this->loginServer());
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ('POST' == $method) {
            if ($body) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
            }
        } else {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }
        $return = curl_exec($ch);

        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if (500 == $statusCode) {
            return false;
        }

        return $return;
    }

    private function loginServer()
    {
        $username = \Drupal::config('recharge_numero.settings')->get('login');
        $password = \Drupal::config('recharge_numero.settings')->get('password');
        $body = [
          'email' => $username,
          'password' => $password,
        ];
        $return = $this->sendRequest('api/v1/login', $body, false, 'POST');
        if ($return) {
            $return = json_decode($return, true);
            return $return['data']['token'] ?? false;
        }
        return false;
    }
}
