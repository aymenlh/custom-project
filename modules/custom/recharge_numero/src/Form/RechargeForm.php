<?php

namespace Drupal\recharge_numero\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form recharge numero for this site.
 */
class RechargeForm extends FormBase
{
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'recharge_numero';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $uri = 'api/v1/products/get-all-products-list?products_type=recharge&page=0&per_page=1000';
        $return = $this->sendRequest($uri, [], true, 'GET');
        $result = json_decode($return, true);
        $optionsProduct = [];
        if(isset($result['status']) && $result['status'] && isset($result['data']['products']['elements']) &&  count($result['data']['products']['elements'])>0){
          $products = $result['data']['products']['elements'];
          foreach ($products as $key => $value) {
            $optionsProduct[$value['id']] = $value['title'];
          }
        }
        $optionsTypeRecharge = [
          'recharge_numero' => $this->t('Recharger mon numéro'),
        ];
        if(count($optionsProduct)>0){
          $optionsTypeRecharge['recharge_nejma'] = $this->t('Recharge Nejma');
        }
        $form['type_recharge'] = [
        '#type' => 'select',
        '#title' => $this->t('Recharge'),
        '#required' => true,
        '#options' => $optionsTypeRecharge,
        '#default_value' => 'recharge_numero',
        '#prefix' => '<div class="row"><div class="col-md-6">',
        '#suffix' => '</div></div>',
    ];
        $form['phone'] = [
      '#type' => 'number',
      '#required' => true,
      '#title' => $this->t('numéro de téléphone'),
      '#placeholder' => $this->t('Saisir le numéro'),
      '#default_value' => $this->config('recharge_numero.settings')->get('phone'),
      '#prefix' => '<div class="row"><div class="col-md-6">',
      '#suffix' => '</div>',
    ];
        $form['phone_confirm'] = [
        '#type' => 'number',
        '#required' => true,
        '#title' => $this->t('Confirmer le numéro de téléphone'),
        '#placeholder' => $this->t('Confirmer le numéro'),
        '#default_value' => $this->config('recharge_numero.settings')->get('phone_confirm'),
        '#prefix' => '<div class="col-md-6">',
        '#suffix' => '<div id="message-phone"></div></div></div>',
    ];
        $form['montant'] = [
        '#type' => 'number',
     //   '#title' => $this->t('Montant de la recharge'),
        '#placeholder' => $this->t('Montant de la recharge'),
        //'#default_value' => $this->config('recharge_numero.settings')->get('montant'),
        '#prefix' => '<div class="row" id="box_montant"><div class="col-md-6">',
        '#suffix' => '<div class="montant-icon">'.$this->t('MRU').'</div><div id="message-montant"></div></div></div>',
        '#states' => [
            'visible' => [
                ':input[name="type_recharge"]' => [
                    ['value' => 'recharge_numero'],
                ],
            ],
            'required' => [
                ':input[name="type_recharge"]' => [
                      ['value' => 'recharge_numero'],
                  ],
              ],
        ],
    ];

      $form['produit_nejma'] = [
          '#type' => 'select',
          '#placeholder' => $this->t('Moyen de paiement'),
          //'#options' => ['all' => $this->t('Choissisez votre produit'), 'masrvi' => $this->t('MASRVI'), 'gimtel' => $this->t('GIMTEL')],
          '#options' => $optionsProduct,
          '#default_value' => 'masrvi',
          '#prefix' => '<div class="row" id="box_produit_nejma"><div class="col-md-6">',
          '#suffix' => '<div id="message-produit_nejma"></div></div></div>',
          '#states' => [
            'visible' => [
                ':input[name="type_recharge"]' => [
                    ['value' => 'recharge_nejma'],
                ],
            ],
            'required' => [
                ':input[name="type_recharge"]' => [
                      ['value' => 'recharge_nejma'],
                  ],
              ],
            ],
      ];
      $form['moyen_paiement'] = [
        '#type' => 'select',
        '#placeholder' => $this->t('Moyen de paiement'),
        '#options' => [/*'all' => t('Moyen de paiement') ,*/ 'masrvi' => $this->t('MASRVI'), 'gimtel' => $this->t('GIMTEL')],
        '#default_value' => 'masrvi',
        '#prefix' => '<div class="row"><div class="col-md-6">',
        '#suffix' => '</div>',
    ];
      $form['email'] = [
        '#type' => 'textfield',
        //    '#title' => $this->t('Votre adresse email'),
        '#placeholder' => $this->t('Votre adresse email'),
        '#prefix' => '<div class="col-md-6">',
        '#suffix' => '<div id="message-email"></div></div></div>',
        '#states' => [
          'visible' => [
              ':input[name="moyen_paiement"]' => [
                  ['value' => 'gimtel'],
              ],
          ],
          'required' => [
              ':input[name="moyen_paiement"]' => [
                    ['value' => 'gimtel'],
                ],
            ],
          ],
      ];

        $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Je valide'),
        '#attributes' => ['class' => ['bouton-blue']],
        '#button_type' => 'primary',
    ];
        $form['#attached'] = ['library' => 'recharge_numero/recharge_numero'];

        return [
        '#theme' => 'recharge_form',
        'render element' => $form,
      ];
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        if (empty($form_state->getValue('phone'))) {
            $message = '<div class="error">'.$this->t('Le champ numéro de téléphone est obligatoire.').'</div>';
            $form_state->setErrorByName('phone', $message);
        } elseif ($form_state->getValue('phone') != $form_state->getValue('phone_confirm')) {
            $message = '<div class="error">'.$this->t('Les champs numéro de téléphone doivent être identique.').'</div>';
            $form_state->setErrorByName('phone_confirm', $message);
        } elseif (empty($form_state->getValue('montant')) || floatval($form_state->getValue('montant')) <= 0) {
            $message = '<div class="error">'.$this->t('Le montant doit être supérieur à zéro.').'</div>';
            $form_state->setErrorByName('montant', $message);
        }elseif($form_state->getValue('type_recharge')=='recharge_nejma' && empty($form_state->getValue('produit_nejma')) ){
          $message = '<div class="error">'.$this->t('Merci de choissir le produit à acheter.').'</div>';
          $form_state->setErrorByName('montant', $message);
        } elseif ('gimtel' == $form_state->getValue('moyen_paiement')) {
            if (empty($form_state->getValue('email')) || ! filter_var($form_state->getValue('email'), FILTER_VALIDATE_EMAIL)) {
                $message = '<div class="error">'.$this->t("l'email est obligatoire pour le recharge GIMTEL.").'</div>';
                $form_state->setErrorByName('montant', $message);
            }
        }

        parent::validateForm($form, $form_state);
    }

    private function getDataFormMasrvi($result, $amount)
    {
        $subject = sprintf($this->t('Recharge d\'un montant de %d MRU par carte bancaire'), $amount);
        $notificationUrl = $result['data']['notification_url'].'?pid='.$result['data']['payment_id'];
        $host = \Drupal::request()->getSchemeAndHttpHost();
        $acceptUrl = \Drupal::request()->getRequestUri().'-accept?pid='.$result['data']['payment_id'];
        $cancelUrl = \Drupal::request()->getRequestUri().'-accept?pid='.$result['data']['payment_id'];
        $declineUrl = \Drupal::request()->getRequestUri().'-accept?pid='.$result['data']['payment_id'];

        $acceptUrl = $host.$acceptUrl;
        $cancelUrl = $host.$cancelUrl;
        $declineUrl = $host.$declineUrl;
        $data = [
        'merchantId' => $result['data']['merchant_id'],
        'actionUrl' => $result['data']['action_url'],
        'sessionId' => $result['data']['session_id'],
        'currency' => $result['data']['currency'],
        'amount' => $amount * 100,
        'purchaseref' => $result['data']['payment_transaction'],
        'brand' => $result['data']['brand'],
        'description' => $subject,
        'text' => $subject,
        'notificationUrl' => urlencode($notificationUrl),
        'acceptUrl' => urlencode($acceptUrl),
        'cancelUrl' => urlencode($cancelUrl),
        'declineUrl' => urlencode($$declineUrl),
    ];
    ?>
    <form id="form-paiement"
        action="<?php echo $data['actionUrl']; ?>"
        method="post">
        <input type="hidden" name="sessionid"
            value="<?php echo $data['sessionId']; ?>" />
        <input type="hidden" name="merchantid"
            value="<?php echo $data['merchantId']; ?>" />
        <input type="hidden" name="amount"
            value="<?php echo $data['amount']; ?>" />
        <input type="hidden" name="currency"
            value="<?php echo $data['currency']; ?>" />
        <input type="hidden" name="purchaseref"
            value="<?php echo $data['purchaseref']; ?>" />
        <input type="hidden" name="brand"
            value="<?php echo $data['brand']; ?>" />
        <input type="hidden" name="description"
            value="<?php echo $data['description']; ?>" />
        <input type="hidden" name="accepturl"
            value="<?php echo $data['acceptUrl']; ?>" />
        <input type="hidden" name="cancelurl"
            value="<?php echo $data['cancelUrl']; ?>" />
        <input type="hidden" name="declineurl"
            value="<?php echo $data['declineUrl']; ?>" />
    </form>
    <script type="text/javascript">
        document.getElementById('form-paiement').submit();
    </script>
    <?php
            die();
    }

    public function getDataFormGimtel($result, $amount, $number, $email)
    {
        $subject = sprintf($this->t('Recharge d\'un montant de %d MRU par carte bancaire'), $amount);
        $notificationUrl = $result['data']['notification_url'].'?pid='.$result['data']['payment_id'];
        $host = \Drupal::request()->getSchemeAndHttpHost();
       echo $redirectBackUrl = $host.\Drupal::request()->getRequestUri().'-accept?pid='.$result['data']['payment_id'];
die;
        $data = [
        'actionUrl' => $result['data']['action_url'],
        'currency' => $result['data']['currency'],
        'pspId' => $result['data']['psp_id'],
        'mpiId' => $result['data']['mpi_id'],
        'mcc' => $result['data']['mcc'],
        'merchantKitId' => $result['data']['merchant_kit_id'],
        'authenticationToken' => $result['data']['authentication_token'],
        'recInsTransactionTotalAmount' => $amount,
        'cardAcceptor' => $result['data']['merchant_id'],
        'transactionReference' => $result['data']['payment_transaction'],
        'cardHolderIPAddress' => $_SERVER['REMOTE_ADDR'],
        'countryCode' => $result['data']['country_code'],
        'dateTimeSIC' => $result['data']['date_time_sic'],
        'dateTimeBuyer' => $result['data']['date_time_buyer'],
        'language' => $result['data']['language'],
        'transactionTypeIndicator' => $result['data']['transaction_type_indicator'],
        'cardHolderMailAddress' => $email,
        'cardHolderPhoneNumber' => $number,
        'callBackUrl' => $result['data']['call_back_url'],
        'redirectBackUrl' => $redirectBackUrl,
    ];
//    var_dump($data);die;
?>
<form id="FormPaiement" action="<?php echo $data['actionUrl'];?>" method="post">
    <input type="hidden" name="pspId" value="<?php echo $data['pspId']; ?>">
    <input type="hidden" name="mpiId" value="<?php echo $data['mpiId']; ?>">
    <input type="hidden" name="cardAcceptor" value="<?php echo $data['cardAcceptor']; ?>">
    <input type="hidden" name="mcc" value="<?php echo $data['mcc']; ?>">
    <input type="hidden" name="merchantKitId" value="<?php echo $data['merchantKitId']; ?>">
    <input type="hidden" name="authenticationToken" value="<?php echo $data['authenticationToken']; ?>">
    <input type="hidden" name="transactionReference" value="<?php echo $data['transactionReference']; ?>">
    <input type="hidden" name="transactionAmount" value="<?php echo $data['recInsTransactionTotalAmount']; ?>">
    <input type="hidden" name="currency" value="<?php echo $data['currency']; ?>">
    <input type="hidden" name="cardHolderMailAddress" value="<?php echo $data['cardHolderMailAddress']; ?>">
    <input type="hidden" name="cardHolderPhoneNumber" value="<?php echo $data['cardHolderPhoneNumber']; ?>">
    <input type="hidden" name="cardHolderIPAddress" value="<?php echo $data['cardHolderIPAddress']; ?>">
    <input type="hidden" name="countryCode" value="<?php echo $data['countryCode']; ?>">
    <input type="hidden" name="dateTimeSIC" value="<?php echo $data['dateTimeSIC']; ?>">
    <input type="hidden" name="dateTimeBuyer" value="<?php echo $data['dateTimeBuyer']; ?>">
    <input type="hidden" name="callBackUrl" value="<?php echo $data['callBackUrl']; ?>">
    <input type="hidden" name="redirectBackUrl" value="<?php echo $data['redirectBackUrl']; ?>">
    <input type="hidden" name="language" value="<?php echo $data['language']; ?>">
    <input type="hidden" name="transactionTypeIndicator" value="<?php echo $data['transactionTypeIndicator']; ?>">

</form>
<script type="text/javascript">
    document.getElementById('FormPaiement').submit();
</script>
<?php
       die();
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $amount = $form_state->getValue('montant');
        $number = $form_state->getValue('phone');
        $email = $form_state->getValue('email', NULL);
        $moyen_paiement = $form_state->getValue('moyen_paiement');
        $result = $this->prepareFormPayment($form_state->getValue('montant'), $moyen_paiement, $form_state->getValue('phone'), $form_state->getValue('phone_confirm'));
        $result = json_decode($result, true);
     // var_dump($result);die;
        $data = [];
        if ($result['status']) {
            if ('masrvi' == $moyen_paiement) {
                $this->getDataFormMasrvi($result, $amount);
            }elseif('gimtel' == $moyen_paiement){
                $this->getDataFormGimtel($result, $amount, $number, $email);
            }
        }
       // die('ici');
    }

    private function prepareFormPayment($amount, $payment_method, $phone_number = null, $phone_number_2 = null)
    {
        $dataMattel = [
          'payment_method' => $payment_method,
          'phone_number' => $phone_number, //recharge my number
          'phone_number_2' => $phone_number_2,
          'modal_key' => null,
          'email' => null,
          'ip_address' => $_SERVER['REMOTE_ADDR'],
          'amount' => $amount,
          'action' => 2,
        ];
        $uri = 'api/v1/payment';

        return $this->sendRequest($uri, $dataMattel, true, 'POST');
    }

    private function sendRequest($path, $body = [], $withAuth = true, $method = 'GET')
    {
        $url_endpoint = \Drupal::config('recharge_numero.settings')->get('url_endpoint');
        $uri = $url_endpoint.$path;
        $ch = curl_init($uri);
        $headers = [
          'source: web',
          'Content-Type: application/json',
          'Accept: application/json',
          'language: fr',
        ];
        if ($withAuth) {
            array_push($headers, 'Authorization: Bearer '.$this->loginServer());
        }
     /*   var_dump($uri);
        var_dump($headers);*/
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ('POST' == $method) {
            if ($body) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
            }
        } else {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        }
        $return = curl_exec($ch);

        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if (500 == $statusCode) {
            return false;
        }

        return $return;
    }

    private function loginServer()
    {
        $username = \Drupal::config('recharge_numero.settings')->get('login');
        $password = \Drupal::config('recharge_numero.settings')->get('password');
        $body = [
          'email' => $username,
          'password' => $password,
        ];
        $return = $this->sendRequest('api/v1/login', $body, false, 'POST');
        if ($return) {
            $return = json_decode($return, true);
            return $return['data']['token'] ?? false;
        }
        return false;
    }
}

?>
