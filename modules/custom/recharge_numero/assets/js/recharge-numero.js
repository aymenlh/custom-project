/**
 * @file
 * Attaches behaviors for the custom Google Maps.
 */

 (function ($, Drupal) {

    jQuery('#my_form_wrapper #edit-phone-confirm').on("cut", function(e) {
        e.preventDefault();
    });
    jQuery('#my_form_wrapper #edit-phone-confirm').on("copy", function(e) {
        e.preventDefault();
    });
    jQuery('#my_form_wrapper #edit-phone-confirm').on("paste", function(e) {
        e.preventDefault();
    });

   jQuery('#my_form_wrapper #edit-type-recharge').on("change", function (e) {
     if (jQuery(this).val() == 'recharge_nejma') {
       jQuery('#box_montant').hide();
       jQuery('#box_produit_nejma').show();
     } else {
      jQuery('#box_montant').show();
      jQuery('#box_produit_nejma').hide();
      }
        e.preventDefault();
    });

    jQuery('#my_form_wrapper input[type="number"]').bind("keypress", function (evt) {
        if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
        {
            evt.preventDefault();
        }
    });

})(jQuery, Drupal);
