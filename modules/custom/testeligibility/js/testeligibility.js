/**
 * @file
 * Attaches behaviors for the custom Google Maps.
 */

(function ($, Drupal) {


  Drupal.behaviors.testeligibilite = {
    attach: function (context, settings) {
      jQuery(document, context).once('testeligibilite').each(function () {
        $('#adresses').select2({
          allowClear: false,
          ajax: {
            type: 'GET',
            url: drupalSettings.path.currentPath + '/search',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: function (params) {
              return {
                term: params.term || '',
                page: params.page || 1,
                rows: 20
              }
            },
            processResults: function (res, params) {
              if (res.status == 'ok') {
                var adresse = res.data.data;
                params.page = params.page || 1;
                var data = [],
                  i;
                for (i = 0; i < adresse.length; i++) {
                  data.push({
                    id: adresse[i].id,
                    text: adresse[i].name
                  });
                }

                return {
                  results: data,
                  pagination: {
                    more: params.page < res.data.last_page,
                  }
                };

              } else {
                console.log('Failed to fetch the addresse');
              }
            }
          }
        });
        $('#adresses').on('change', function () {
          $('.message_eligibility').html('');
        });
        $('#check_eligibilite').on('click', function () {
          $(".lds-dual-ring").css('display','inline-block');
          if ($('#adresses').val()) {
            $.ajax({
              type: 'GET',
              dataType: "json",
              url: drupalSettings.path.currentPath + '/testAdress?idAdresse=' + $('#adresses').val(),
              success: function (data) {
                if (data.status == 'ok') {
                  $('.message_eligibility').html('<span class="success">'+data.message+'</span>')
                } else {
                  $('.message_eligibility').html('<span class="echec">'+data.message+'</span>')
                }
                $(".lds-dual-ring").hide();
              }
            });
          }

        });
      });
    }
  };
})(jQuery, Drupal);
