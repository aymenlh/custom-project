<?php

namespace Drupal\testeligibility\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for testeligibility routes.
 */
class TesteligibilityController extends ControllerBase
{
    /**
     * Builds the response.
     */
    public function build()
    {
        // var_dump($this->loginServer());die;
        $build['content'] = [
      '#theme' => 'testeligibility_page',
      '#custom_text' => $this->t('Implement method: CustomPage using a template'),
      '#attached' => ['library' => 'testeligibility/testeligibility'],
    ];

        return $build;
    }

  private function sendRequest($path,$body=[],$withAuth=true,$method='GET'){
      $url_endpoint = \Drupal::config('testeligibility.settings')->get('url_endpoint');
      $uri = $url_endpoint.$path;
      $ch = curl_init($uri);
      $headers = [
        'source: web',
        'Content-Type: application/json',
        'Accept: application/json',
      ];
      if($withAuth){
        array_push($headers,'Authorization: Bearer '.$this->loginServer());
      }

      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      if($method == 'POST'){
        if($body){
          curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
        }
      }else{
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
      }


        $return = curl_exec($ch);

        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if (500 == $statusCode) {
          return false;
        }
      return $return;

  }

    public function searchAdress()
    {
        $term = \Drupal::request()->query->get('term');
        $page = \Drupal::request()->query->get('page');
        $rows = \Drupal::request()->query->get('rows');
        $uri = 'api/v1/addresses/get-address?page='.$page.'&rows='.$rows.'&term='.urlencode($term);
        $return = $this->sendRequest($uri,[],true,'GET');
        $result = [
          'status' => 'ko',
          'data' => [],
        ];
        if ($return) {
          $res =   json_decode($return,true);
          if(isset($res['data']['data'])){
            $result['status'] = 'ok';
            $result['data'] = $res['data'];
          }
        }
        echo json_encode($result);die;
    }

    public function testAdress()
    {
      $result = [
        'status' => 'ko',
        'message' => $this->t("Vous n'êtes pas éligible1"),
      ];
        $idAdresse = \Drupal::request()->query->get('idAdresse');

        if(!$idAdresse || intval($idAdresse) <=0) {
          echo json_encode($result);die;
        }
        $uri = 'api/v1/eligibility/'.$idAdresse.'/get-eligibility-by-address';
        $return = $this->sendRequest($uri,[],true,'GET');
 
        if ($return) {
          $res =   json_decode($return,true);
          if(isset($res['data']['eligibilty'])){
            $result['status'] = ($res['data']['eligibilty'])?'ok':'ko';
            $result['message'] = $res['message'];
          }
        }
        echo json_encode($result);die;
    }

    private function loginServer()
    {
        $username = \Drupal::config('testeligibility.settings')->get('username');
        $password = \Drupal::config('testeligibility.settings')->get('password');
        $body = [
          'email' => $username,
          'password' => $password,
        ];
        $return = $this->sendRequest('api/v1/login',$body,false,'POST');
        if ($return) {
          $return = json_decode($return, true);
          return $return['data']['token'] ?? false;
        }
        return false;

    }
}
