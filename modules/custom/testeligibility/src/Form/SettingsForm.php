<?php

namespace Drupal\testeligibility\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure testeligibility settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'testeligibility_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['testeligibility.settings'];
  }

  public function getSettings(){
    return $this->config('testeligibility.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['url_endpoint'] = [
      '#type' => 'url',
      '#title' => $this->t('Url endpoint'),
      '#default_value' => $this->config('testeligibility.settings')->get('url_endpoint'),
    ];
    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User name'),
      '#default_value' => $this->config('testeligibility.settings')->get('username'),
    ];
    $form['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $this->config('testeligibility.settings')->get('password'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty($form_state->getValue('url_endpoint'))) {
      $form_state->setErrorByName('url_endpoint', $this->t('url endpoint value is required.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('testeligibility.settings')
    ->set('url_endpoint', $form_state->getValue('url_endpoint'))
    ->set('username', $form_state->getValue('username'))
    ->set('password', $form_state->getValue('password'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
