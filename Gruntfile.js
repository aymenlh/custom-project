
module.exports = function(grunt) {
    const sass = require('node-sass');
    grunt.initConfig({
      /*********
       *  disabled
       * ********
       concat: {
          css: {
              src: ['./themes/custom/customtheme/css/*.css'],
              dest: './themes/custom/customtheme/css/styles.css'
          }
      },
    */
      sass: {
        options: {
            sourceMap: true,
            outputStyle: 'compressed',
            implementation: sass //and this part
		    },
        build: {
            files: [{
                src: './themes/custom/customtheme/sass/style.scss',
                dest: './themes/custom/customtheme/css/styles.css'
            }]
        }
        ,
        build_ar: {
          files: [{
              src: './themes/custom/customtheme/sass/style-ar.scss',
              dest: './themes/custom/customtheme/css/styles_ar.css'
          }]
      }
    },
      watch: {
        styles: {
          files: '**/*.scss', 
          tasks: ['sass:build','sass:build_ar' ]
           
        },}

    });
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch')
  }